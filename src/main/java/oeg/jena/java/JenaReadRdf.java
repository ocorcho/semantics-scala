package oeg.jena.java;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.VCARD;

public class JenaReadRdf {
	private Model model = null;
	private static String prefix="http://oeg-upm.net/data#";
	public JenaReadRdf(){
      model = ModelFactory.createDefaultModel();
	}
	
	public void generatePersons(){
		String[] names=new String[]{"Jose","Freddy","Esther","Lucas","Eulogio","Mike","Kelly"};
		for (String name : names) {
			Resource person = model.createResource(prefix+name);
			person.addLiteral(VCARD.FN, name);
		}
	}
	
	
	
	
	
	
	
	
	

}
